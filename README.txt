Foram criados dois Package

teste.model:
Criei a classe People com variáveis privadas:

Firstname - Primeiro nome
Lastname - Sobrenome
Weight - Peso
Height - Altura

 Foram criados os Getters e Setters para
fazer as alterações e leituras necessárias quando a classe for instanciada.

teste.main...
 Esse package foi criado para armazenar a classe Main onde foi instanciado 
a classe People com o nome people.
foi criado variáveis String com nomes semelhantes aos  atributos 
criados em People, nelas armazenei o valor coletado pelo Scanner e atribui people.

 Criei uma variável com o nome IMC que recebe os valores amazenados  
em peso e altura, fiz o calculo utilizando o metodo Math.pow onde pude 
multiplicar a altura vezes ela mesmo divido pelo peso.

FileWriter:
 A função me permite criar um arquivo txt, podendo ser utilizado apenas no Try.
Após [MeuNomeCompleto.txt] foi adicionado um true para que as informações que 
forem printadas não sobreescreva a anterior, conexão foi fechada após o print.

PrintWriter:
 Função me permite gravar os prints que forem feitos no console dentro do 
file, passando o file como parametro.
Utilizei os getters para pegar o valor de people, o método toUperCase
para serem imprimidos em maiúsculo e o "%.2f" para determinar
a quantidade de casas decimais.

 Utilizei o printW.printf para escrever dentro do txt e flush para forçar e garantir a 
 escrita dentro do txt, utilizei o método toUperCase para serem gravadas em maiúsculas 
 o "%.2f" para determinar quantidade de casas decimais e por ultimo fechei flush a conexão 
 do print e do FileWriter.

 Por último utilizei o Do While para solicitar se deseja continuar.


