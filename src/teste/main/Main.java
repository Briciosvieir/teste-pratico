package teste.main;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import javax.swing.JOptionPane;
import teste.model.People;


public class Main {




	public static void main(String[] args) {
		
		
		
		String comeback= "";

		do {

			
		People  people = new People();	
		Scanner scan = new Scanner(System.in);
		
		
		System.out.print("Primeiro nome: ");
		String firstname = scan.nextLine();
		people.setFirstname(firstname);
		
		System.out.print("Sobrenome: ");
		String lastname = scan.nextLine();
		people.setLastname(lastname);	
		
		System.out.print("Seu peso: ");
		double weight = scan.nextDouble();
		people.setWeight(weight);
		
		System.out.print("Sua altura: ");
		double height = scan.nextDouble();
		people.setHeight(height);
		System.out.println();
		
		double imc = people.getWeight() / Math.pow(people.getHeight(),2);
		System.out.printf(people.getFirstname().toUpperCase()+" "+people.getLastname().toUpperCase()+" "+"%.2f ",imc);
		System.out.println();

		
			FileWriter file;
			try {
				file = new FileWriter("FabricioVieiraDeSousa.txt",true);
				PrintWriter printW = new PrintWriter(file);
				
				printW.printf(people.getFirstname().toUpperCase()+" "+people.getLastname().toUpperCase()+" "+"%.2f\n",imc);
				printW.flush();
				printW.close();
				file.close();
				
				
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(null, "Não foi possível gravar");
			}
		
			System.out.println();
			System.out.printf("#### DESEJA COLETAR NOVOS DADOS ? [ S/N ]: "); 
				comeback = scan.next();

		}while(comeback.equalsIgnoreCase("s"));		
		
	}
		
	

}
